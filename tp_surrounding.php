<?php
include "connection.php";


$latitude = $_GET["lat"];
$longitude = $_GET["long"];


$sql = mysqli_query($con,"SELECT *, 
    (6371 * acos(cos(radians('$latitude'))
        * cos(radians(tbg_latitude))
        * cos(radians(tbg_longitude) - radians('$longitude'))
            + sin(radians('$latitude') )
        * sin(radians(tbg_latitude)))
    ) AS distance FROM tp_surrounding ORDER BY distance LIMIT 3");

$result = array();

while($row = mysqli_fetch_array($sql)){
    array_push($result,array(
        'id' => $row['id'],
        'tp_name' => $row['tp_name'],
        'tbg_latitude' => $row['tbg_latitude'],
        'tbg_longitude' => $row['tbg_longitude'],
        'distance' => $row['distance'],
        'tp_height' => $row['tower_height']
    ));
}

echo json_encode(array('result'=>$result));

mysqli_close($con);


?>
