<?php
include "connection.php";


$latitude = $_GET["lat"];
$longitude = $_GET["long"];


$sql = mysqli_query($con,"SELECT *, 
    (6371 * acos(cos(radians('$latitude'))
        * cos(radians(latitude_cell))
        * cos(radians(longitude_cell) - radians('$longitude'))
            + sin(radians('$latitude') )
        * sin(radians(latitude_cell)))
    ) AS distance FROM celluler_surrounding ORDER BY distance LIMIT 3");

$result = array();

while($row = mysqli_fetch_array($sql)){
    array_push($result,array(
        'id' => $row['id'],
        'site_id' => $row['site_id'],
        'site_name' => $row['site_name'],
        'latitude_cell' => $row['latitude_cell'],
        'longitude_cell' => $row['longitude_cell'],
        'distance' => $row['distance'],
        'owner' => $row['owner']
    ));
}

echo json_encode(array('result'=>$result));

mysqli_close($con);


?>
