<?php
include "connection.php";


$id = $_GET["desa"];

$sql = mysqli_query($con,"SELECT * FROM tutela_bencmark WHERE id_desa = '$id'");

$result = array();

while($row = mysqli_fetch_array($sql)){
    array_push($result,array(
        'id' => $row['id'],
        'skor_indosat' => $row['skor_indosat'],
        'skor_xl' => $row['skor_xl'],
        'skor_smartfren' => $row['skor_smartfren'],
        'skor_telkomsel' => $row['skor_telkomsel'],
        'skor_three' => $row['skor_three'],
        'dwn_thrght_isat' => $row['dwn_thrght_isat'],
        'dwn_thrght_smrtfren' => $row['dwn_thrght_smrtfren'],
        'dwn_thrght_xl' => $row['dwn_thrght_xl'],
        'dwn_thrght_telsel' => $row['dwn_thrght_telsel'],
        'dwn_thrght_3' => $row['dwn_thrght_3'],
        'upld_thrght_isat' => $row['upld_thrght_isat'],
        'upld_thrght_smrtfren' => $row['upld_thrght_smrtfren'],
        'upld_thrght_xl' => $row['upld_thrght_xl'],
        'upld_thrght_telsel' => $row['upld_thrght_telsel'],
        'upld_thrght_3' => $row['upld_thrght_3'],
        'latency_isat' => $row['latency_isat'],
        'latency_smrtfren' => $row['latency_smrtfren'],
        'latency_xl' => $row['latency_xl'],
        'latency_telsel' => $row['latency_telsel'],
        'latency_3' => $row['latency_3'], 
        'id_desa' => $row['id_desa']
    ));
}

echo json_encode(array('result'=>$result));

mysqli_close($con);


?>
